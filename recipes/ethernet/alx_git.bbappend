FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

# Fix for the broken "make clean" command
CLEANBROKEN = "1"
