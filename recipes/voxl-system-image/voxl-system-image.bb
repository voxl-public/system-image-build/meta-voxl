DESCRIPTION = "voxl system image meta package"
SUMMARY     = "Meta package that is used to track system image version for DEPENDS in ipk packages"
LICENSE     = "CLOSED"

# Package version should match system image version
PV = "4.0"

ALLOW_EMPTY_${PN} = "1"

# used by software packages to determine platform
RPROVIDES_${PN} = "cci-direct-support"

do_package_qa[noexec] = "1"
