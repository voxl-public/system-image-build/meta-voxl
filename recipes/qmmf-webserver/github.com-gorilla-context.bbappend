FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

# Set a specific commit hash rather than grabbing the latest
SRCREV = "aed02d124ae4a0e94fea4541c8effd05bf0c8296"
