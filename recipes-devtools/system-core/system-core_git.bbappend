voxl_fix_usb_composition() {
    echo "Starting HSIC HSUSB processing"
    if [ ! -f ${S}/usb/compositions/hsusb_next ]; then
	echo "hsusb_next doesn't exist, creating it"
        echo "9025" > ${S}/usb/compositions/hsusb_next
    else
	echo "hsusb_next already exists"
    fi
    if [ ! -f ${S}/usb/compositions/hsic_next ]; then
	echo "hsic_next doesn't exist, creating it"
        echo "0" > ${S}/usb/compositions/hsic_next
    else
	echo "hsic_next already exists"
    fi
    ls -l ${S}/usb/compositions/*_next
}

do_voxl_fix_install_pre() {
    echo "Pre-install fix"
    voxl_fix_usb_composition
}

addtask voxl_fix_install_pre after do_compile before do_install

do_voxl_fix_install_post() {
    echo "Post-install fix"
    voxl_fix_usb_composition
}

addtask voxl_fix_install_post after do_install before do-populate-sysroot
