
do_install_launch_application () {
    bbnote "Adding test-launch with workdir ${WORKDIR}"
    install -d ${WORKDIR}/image/usr/bin
    install -m 0755 ${WORKDIR}/build/examples/.libs/test-launch ${WORKDIR}/image/usr/bin/gst-rtsp-server-launch
}

addtask install_launch_application after do_install before do_package

